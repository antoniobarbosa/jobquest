'use strict';

describe('Controller: Questione3Ctrl', function () {

  // load the controller's module
  beforeEach(module('jobquestApp'));

  var Questione3Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Questione3Ctrl = $controller('Questione3Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(Questione3Ctrl.awesomeThings.length).toBe(3);
  });
});
