'use strict';

describe('Controller: RankingCtrl', function () {

  // load the controller's module
  beforeEach(module('jobquestApp'));

  var RankingCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RankingCtrl = $controller('RankingCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RankingCtrl.awesomeThings.length).toBe(3);
  });
});
