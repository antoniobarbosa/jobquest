'use strict';

describe('Controller: Questione1Ctrl', function () {

  // load the controller's module
  beforeEach(module('jobquestApp'));

  var Questione1Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Questione1Ctrl = $controller('Questione1Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(Questione1Ctrl.awesomeThings.length).toBe(3);
  });
});
