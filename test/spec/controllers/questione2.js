'use strict';

describe('Controller: Questione2Ctrl', function () {

  // load the controller's module
  beforeEach(module('jobquestApp'));

  var Questione2Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Questione2Ctrl = $controller('Questione2Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(Questione2Ctrl.awesomeThings.length).toBe(3);
  });
});
