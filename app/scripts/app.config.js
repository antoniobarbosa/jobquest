'use strict';

/**
 * @ngdoc overview
 * @name jobquestApp
 * @description
 * # jobquestApp
 *
 * Main module of the application.
 */
angular
    .module('jobquestApp').config(function($urlRouterProvider, $stateProvider, $httpProvider){
        $urlRouterProvider.otherwise('/login');
        $stateProvider

            .state('login',{
                url:'/login',
                templateUrl:'views/login.html',
                controller:'LoginCtrl'

            })
            .state('home',{
                url:'/home',
                templateUrl:'views/home.html',
                controller:'HomeCtrl'
            })
            .state('question',{
                url:'/question/:id',
                templateUrl:'views/question.html',
                controller:'QuestionCtrl'

            })
            .state('questione1',{
                url:'/questione1/:id',
                templateUrl:'views/questione1.html',
                controller:'Questione1Ctrl'

            })
            .state('questione2',{
                url:'/questione2',
                templateUrl:'views/questione2.html',
                controller:'Questione2Ctrl'

            })
            .state('questione31',{
                url:'/questione31',
                templateUrl:'views/questione3-1.html',
                controller:'Questione31Ctrl'

            })
                .state('questione32',{
                url:'/questione32',
                templateUrl:'views/questione3-2.html',
                controller:'Questione32Ctrl'

            })
            .state('ranking',{
                url:'/ranking',
                templateUrl:'views/ranking.html',
                controller:'RankingCtrl'
            })

        $httpProvider.interceptors.push(function($q, authToken,$injector) {
            return {
                'request': function(config) {
                    return config;
                },
                'response': function(response) {
                    console.log("request");
                    console.log(response);
                    return response;
                },
                'responseError': function(rejection) {
                    // do something on error
                    authToken.removeToken();
                    var stateService = $injector.get('$state');
                    stateService.go('login');
                    return $q.reject(rejection);
                }
            };
        });
    })

    .constant("API_URL","http://192.168.0.25:26751/");
