'use strict';

/**
 * @ngdoc service
 * @name jobquestApp.authToken
 * @description
 * # authToken
 * Service in the jobquestApp.
 */
angular.module('jobquestApp')
    .factory('authToken', function ($window) {
        var storage = $window.localStorage;
        var cachedToken;
        return{
            getToken:function(){
                if(!cachedToken)
                    cachedToken=storage.getItem('userToken');
                return cachedToken
            },
            setToken:function(token){
                console.log(token)
                cachedToken=token;
                //cachedToken.timeNow = Date.now();
                try {
                    storage.setItem('userToken',token);
                    return true;
                } catch (error) {
                    return false;
                }

            },
            isAuthenticated:function(){
                console.log(!!this.getToken());
                console.log(this.getToken());
                return !!this.getToken();
            },
            removeToken:function(){
                cachedToken = null;
                storage.removeItem('userToken');

                return !!this.getToken();
            }
        }
    });
