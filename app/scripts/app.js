'use strict';

/**
 * @ngdoc overview
 * @name jobquestApp
 * @description
 * # jobquestApp
 *
 * Main module of the application.
 */
angular
  .module('jobquestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ngDraggable'
  ]).run(function() {
    FastClick.attach(document.body);
});;
