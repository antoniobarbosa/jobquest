'use strict';

/**
 * @ngdoc function
 * @name jobquestApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the jobquestApp
 */
angular.module('jobquestApp')
  .controller('LoginCtrl', function ($scope, $http, authToken,$state) {
   $scope.login ={};

   $scope.doLogin = function(){
       //if validação
       $http.post("http://via.events/jogoquest/api/Usuarios/Logar", $scope.login).success(function(res){
           authToken.setToken(res);
           $state.go('home');
       });
   }
  });
