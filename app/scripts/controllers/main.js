'use strict';

/**
 * @ngdoc function
 * @name jobquestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the jobquestApp
 */
angular.module('jobquestApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
