'use strict';

/**
 * @ngdoc function
 * @name jobquestApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the jobquestApp
 */
angular.module('jobquestApp')
    .controller('HomeCtrl', function ($http,$scope,$state,authToken) {
        $scope.texto1=""
        $scope.showTexto1=false;
        $scope.showTexto2=false;
        $scope.showTexto3=false;
        $scope.showTexto4=false;
        $scope.userStatus={
            UltimaPerguntaRespondidaFront:30
        };
        $scope.removeShow = function(texto) {
            if(texto==1){
                $scope.showTexto1=false;
                $scope.showTexto2=true;
            }else if(texto==2){
                $scope.showTexto2=false;
                $scope.showTexto3=true;
            }else if(texto==3){
                $scope.showTexto3=false;
            }  else if(texto==4){
                $scope.showTexto4=false;
                $scope.showTexto5=true;
            }
        };
        if(!authToken.isAuthenticated()){
            $state.go('login');

        }else{
            $http.get('http://via.events/jogoquest/api/Usuarios/ObterUsuario/'+authToken.getToken()).success(function(res){

                console.log(res);
                res.UltimaPerguntaRespondida = (res.UltimaPerguntaRespondida<=200||res.UltimaPerguntaRespondida==null||res.UltimaPerguntaRespondida=='E5'||
                    res.UltimaPerguntaRespondida=='E1'
                    ||res.UltimaPerguntaRespondida=='E2')?200:res.UltimaPerguntaRespondida;
                $scope.userStatus=res;
                $scope.showTexto1=(res.UltimaPerguntaRespondida<=200||res.UltimaPerguntaRespondida==null||res.UltimaPerguntaRespondida=='E5'||
                    res.UltimaPerguntaRespondida=='E1'
                    ||res.UltimaPerguntaRespondida=='E2')
                $scope.texto1="";

                $scope.userStatus.UltimaPerguntaRespondidaFront=parseFloat($scope.userStatus.UltimaPerguntaRespondida)
                if($scope.userStatus.UltimaPerguntaRespondida=='2E1.2'){
                    $scope.userStatus.UltimaPerguntaRespondidaFront=209;
                }
                if($scope.userStatus.UltimaPerguntaRespondida=='2E1.1'){
                    $scope.userStatus.UltimaPerguntaRespondidaFront=208.5;
                }


                if($scope.userStatus.UltimaPerguntaRespondida=='209'){
                    $scope.userStatus.UltimaPerguntaRespondidaFront=208.5;
                }
                if($scope.userStatus.UltimaPerguntaRespondida=='219'){
                    $scope.userStatus.UltimaPerguntaRespondidaFront=218.5;
                }

                if($scope.userStatus.UltimaPerguntaRespondida=='2E2.1'){
                    $scope.userStatus.UltimaPerguntaRespondidaFront=219;
                }


                if($scope.userStatus.UltimaPerguntaRespondida=='227'){
                    $scope.userStatus.UltimaPerguntaRespondidaFront=226.5;

                }
                if($scope.userStatus.UltimaPerguntaRespondida=='2E3.1'){
                    $scope.userStatus.UltimaPerguntaRespondidaFront=230;
                    $scope.showTexto4=true;
                }

                $scope.abrePergunta = function(perguntaId){
                    console.log(perguntaId);
                    if( $scope.userStatus.UltimaPerguntaRespondidaFront==208.5&&perguntaId=='2E1'){
                        console.log('$scope.userStatus.UltimaPerguntaRespondida');
                        console.log($scope.userStatus.UltimaPerguntaRespondida=='2E1.1');
                        if($scope.userStatus.UltimaPerguntaRespondida=='2E1.1'){
                            console.log("testeasdiasjdis");
                            $state.go('questione1',{ id: 2})
                        }else{
                            $state.go('questione1',{ id: 1})
                        }
                    }
                    if( $scope.userStatus.UltimaPerguntaRespondidaFront==218.5&&perguntaId=='2E2'){

                        $state.go('questione2')
                    }

                    if( $scope.userStatus.UltimaPerguntaRespondidaFront==226.5&&perguntaId=='2E5'){
                        $state.go('questione3');
                    }

                    if(perguntaId==(parseFloat($scope.userStatus.UltimaPerguntaRespondidaFront)+1)){
                        $state.go('question', { id: perguntaId})
                    }else{

                    }
                }
            })
        }
        $scope.logout=function(){
            authToken.removeToken();
            $state.go('login')
        };

    });
