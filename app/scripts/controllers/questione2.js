/**
 * @ngdoc function
 * @name jobquestApp.controller:Questione2Ctrl
 * @description
 * # Questione2Ctrl
 * Controller of the jobquestApp
 */
angular.module('jobquestApp')
    .controller('Questione2Ctrl',function ($scope,$http, $state,authToken) {
        var imagemAtual=0;
        $scope.pergunta={};
        $scope.showTexto7=false;
        $scope.remove = function() {
            $scope.showTexto7=false;
        };
        $scope.bgClass=function(){
            if (imagemAtual<=2){
                return "game-2-"+imagemAtual;
            }else{
                return "game-none";
            }
        }
        $scope.changeTexto = function(){
            imagemAtual++;
        }
        $scope.centerAnchor = true;
        $scope.toggleCenterAnchor = function () {$scope.centerAnchor = !$scope.centerAnchor}
        $scope.droppedObjects1 = [];
        $scope.droppedObjects2= [];
        $scope.droppedObjects3= [];
        $scope.droppedObjects4= [];
        $scope.droppedObjects5= [];
        $scope.droppedObjects6= [];
        $scope.droppedObjects7= [];
        $scope.droppedObjects7e5= [];
        $scope.droppedObjects8= [];
        $scope.droppedObjects9= [];
        $scope.droppedObjects10= [];
        $scope.droppedObjects11= [];
        $scope.droppedObjects12= [];
        $scope.droppedObjects13= [];
        $http.get('http://via.events/jogoquest/api/Perguntas/ObterPorId/2E2.1/'+authToken.getToken())
            .success(function(res){
                $scope.pergunta=res;
                console.log($scope.pergunta)
                $scope.droppedObjects1=$scope.pergunta.Alternativas;
                angular.forEach($scope.droppedObjects1,function(value, index){
                    value.insideContainer='droppedObjects1';
                })
            })
        $scope.onDropComplete1=function(data,evt){
            var index = $scope.droppedObjects1.indexOf(data);
            if (index == -1&&data){
                data.insideContainer='droppedObjects1';
                $scope.droppedObjects1.push(data);
            }
        }
        $scope.onDragSuccess1=function(data,evt){

            var index = $scope.droppedObjects1.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects1.splice(index, 1);
            }
        }
        $scope.onDragSuccess2=function(data,evt){
            var index = $scope.droppedObjects2.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects2.splice(index, 1);
            }
        }
        $scope.onDropComplete2=function(data,evt){
            if($scope.droppedObjects2[0]&&data.insideContainer!='droppedObjects2'){
                $scope.droppedObjects2[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects2[0]);
                $scope.droppedObjects2.splice(0, 1);
            }
            data.insideContainer='droppedObjects2';
            $scope.droppedObjects2.push(data);
        }

        $scope.onDragSuccess3=function(data,evt){
            var index = $scope.droppedObjects3.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects3.splice(index, 1);
            }
        }
        $scope.onDropComplete3=function(data,evt){
            if($scope.droppedObjects3[0]&&data.insideContainer!='droppedObjects3'){
                $scope.droppedObjects3[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects3[0]);
                $scope.droppedObjects3.splice(0, 1);
            }
            data.insideContainer='droppedObjects3';
            $scope.droppedObjects3.push(data);
        }

        $scope.onDragSuccess4=function(data,evt){
            var index = $scope.droppedObjects4.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects4.splice(index, 1);
            }
        }
        $scope.onDropComplete4=function(data,evt){
            if($scope.droppedObjects4[0]&&data.insideContainer!='droppedObjects4'){
                $scope.droppedObjects4[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects4[0]);
                $scope.droppedObjects4.splice(0, 1);
            }
            data.insideContainer='droppedObjects3';
            $scope.droppedObjects4.push(data);
        }

        $scope.onDragSuccess5=function(data,evt){
            var index = $scope.droppedObjects5.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects5.splice(index, 1);
            }
        }
        $scope.onDropComplete5=function(data,evt){
            if($scope.droppedObjects5[0]&&data.insideContainer!='droppedObjects5'){
                $scope.droppedObjects5[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects5[0]);
                $scope.droppedObjects5.splice(0, 1);
            }
            data.insideContainer='droppedObjects5';
            $scope.droppedObjects5.push(data);
        }
        $scope.onDragSuccess6=function(data,evt){
            var index = $scope.droppedObjects6.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects6.splice(index, 1);
            }
        }
        $scope.onDropComplete6=function(data,evt){
            if($scope.droppedObjects6[0]&&data.insideContainer!='droppedObjects6'){
                $scope.droppedObjects6[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects6[0]);
                $scope.droppedObjects6.splice(0, 1);
            }
            data.insideContainer='droppedObjects6';
            $scope.droppedObjects6.push(data);
        }

        $scope.onDragSuccess7=function(data,evt){
            var index = $scope.droppedObjects7.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects7.splice(index, 1);
            }
        }
        $scope.onDropComplete7=function(data,evt){
            if($scope.droppedObjects7[0]&&data.insideContainer!='droppedObjects7'){
                $scope.droppedObjects7[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects7[0]);
                $scope.droppedObjects7.splice(0, 1);
            }
            data.insideContainer='droppedObjects7';
            $scope.droppedObjects7.push(data);
        }

        $scope.onDragSuccess7e5=function(data,evt){
            var index = $scope.droppedObjects7e5.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects7e5.splice(index, 1);
            }
        }
        $scope.onDropComplete7e5=function(data,evt){
            if($scope.droppedObjects7e5[0]&&data.insideContainer!='droppedObjects7e5'){
                $scope.droppedObjects7e5[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects7e5[0]);
                $scope.droppedObjects7e5.splice(0, 1);
            }
            data.insideContainer='droppedObjects7e5';
            $scope.droppedObjects7e5.push(data);
        }


        $scope.onDragSuccess8=function(data,evt){
            var index = $scope.droppedObjects8.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects8.splice(index, 1);
            }
        }
        $scope.onDropComplete8=function(data,evt){
            if($scope.droppedObjects8[0]&&data.insideContainer!='droppedObjects8'){
                $scope.droppedObjects8[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects8[0]);
                $scope.droppedObjects8.splice(0, 1);
            }
            data.insideContainer='droppedObjects8';
            $scope.droppedObjects8.push(data);
        }

        $scope.onDragSuccess9=function(data,evt){
            var index = $scope.droppedObjects9.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects9.splice(index, 1);
            }
        }
        $scope.onDropComplete9=function(data,evt){
            if($scope.droppedObjects9[0]&&data.insideContainer!='droppedObjects9'){
                $scope.droppedObjects9[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects9[0]);
                $scope.droppedObjects9.splice(0, 1);
            }
            data.insideContainer='droppedObjects9';
            $scope.droppedObjects9.push(data);
        }

        $scope.onDragSuccess10=function(data,evt){
            var index = $scope.droppedObjects10.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects10.splice(index, 1);
            }
        }
        $scope.onDropComplete10=function(data,evt){
            if($scope.droppedObjects10[0]&&data.insideContainer!='droppedObjects10'){
                $scope.droppedObjects10[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects10[0]);
                $scope.droppedObjects10.splice(0, 1);
            }
            data.insideContainer='droppedObjects10';
            $scope.droppedObjects10.push(data);
        }

        $scope.onDragSuccess11=function(data,evt){
            var index = $scope.droppedObjects11.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects11.splice(index, 1);
            }
        }
        $scope.onDropComplete11=function(data,evt){
            if($scope.droppedObjects11[0]&&data.insideContainer!='droppedObjects11'){
                $scope.droppedObjects11[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects11[0]);
                $scope.droppedObjects11.splice(0, 1);
            }
            data.insideContainer='droppedObjects11';
            $scope.droppedObjects11.push(data);
        }

        $scope.onDragSuccess12=function(data,evt){
            var index = $scope.droppedObjects12.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects12.splice(index, 1);
            }
        }
        $scope.onDropComplete12=function(data,evt){
            if($scope.droppedObjects12[0]&&data.insideContainer!='droppedObjects12'){
                $scope.droppedObjects12[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects12[0]);
                $scope.droppedObjects12.splice(0, 1);
            }
            data.insideContainer='droppedObjects12';
            $scope.droppedObjects12.push(data);
        }

        $scope.onDragSuccess13=function(data,evt){
            var index = $scope.droppedObjects13.indexOf(data);
            if (index > -1) {
                $scope.droppedObjects13.splice(index, 1);
            }
        }
        $scope.onDropComplete13=function(data,evt){
            if($scope.droppedObjects13[0]&&data.insideContainer!='droppedObjects13'){
                $scope.droppedObjects13[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.droppedObjects13[0]);
                $scope.droppedObjects13.splice(0, 1);
            }
            data.insideContainer='droppedObjects13';
            $scope.droppedObjects13.push(data);
        }



        var inArray = function(array, obj) {
            var index = array.indexOf(obj);
        }
        $scope.responderE1= function(){
            function sortNumber(a,b) {
                return a - b;
            }
            console.log($scope.droppedObjects1[0]);

            var resposta = "";
            resposta+=($scope.droppedObjects2[0].Valor==1||$scope.droppedObjects2[0].Valor==2)? '1':$scope.droppedObjects2[0].Valor;
            resposta+=",";
            resposta+=($scope.droppedObjects3[0].Valor==1||$scope.droppedObjects3[0].Valor==2)? '2':$scope.droppedObjects3[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.droppedObjects4[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.droppedObjects5[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.droppedObjects6[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.droppedObjects7[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.droppedObjects7e5[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.droppedObjects8[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.droppedObjects9[0].Valor;
            resposta+=",";
            resposta+=($scope.droppedObjects10[0].Valor==10||$scope.droppedObjects10[0].Valor==11||$scope.droppedObjects10[0].Valor==12)? '10':$scope.droppedObjects10[0].Valor;
            resposta+=",";
            resposta+=($scope.droppedObjects11[0].Valor==10||$scope.droppedObjects11[0].Valor==11||$scope.droppedObjects11[0].Valor==12)? '11':$scope.droppedObjects11[0].Valor;
            resposta+=",";
            resposta+=($scope.droppedObjects12[0].Valor==10||$scope.droppedObjects12[0].Valor==11||$scope.droppedObjects12[0].Valor==12)? '12':$scope.droppedObjects12[0].Valor;
            resposta+=",";
            resposta+=$scope.droppedObjects13[0].Valor;


            var respostaPrincipal = {
                "NumeroPergunta":  $scope.pergunta.Numero,
                "ValorAlternativaRespondida": resposta
            };
            console.log(respostaPrincipal)
            $http.post('http://via.events/jogoquest/api/Perguntas/Responder/'+authToken.getToken(),
                    respostaPrincipal).success(function(){
                        $state.go('home');

                })

        }
    });

