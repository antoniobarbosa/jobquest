'use strict';

/**
 * @ngdoc function
 * @name jobquestApp.controller:QuestionCtrl
 * @description
 * # QuestionCtrl
 * Controller of the jobquestApp
 */
angular.module('jobquestApp')
  .controller('QuestionCtrl', function ($stateParams,authToken,$scope,$http,$state) {
        console.log($stateParams.id)
        $scope.alternativaSelecionada = {}
        $scope.selecionouAlternativa=false;
        $scope.pergunta={};

        $http.get('http://via.events/jogoquest/api/Perguntas/ObterPorId/'+$stateParams.id+'/'+authToken.getToken()).success(function(res){
            $scope.pergunta=res;
            console.log(res);
        })
        $scope.selecionaAlternativa = function(alternativa){
            $scope.alternativaSelecionada = alternativa;
            $scope.selecionouAlternativa=true;
        }
        $scope.enviar = function(){
            if($scope.alternativaSelecionada!={}){
                $http.post('http://via.events/jogoquest/api/Perguntas/Responder/'+authToken.getToken(),
                {
                    "NumeroPergunta":  $scope.pergunta.Numero,
                    "ValorAlternativaRespondida": $scope.alternativaSelecionada.Valor
                }).success(function(){
                        $state.go('home');
                    })

            }
        }

  });
