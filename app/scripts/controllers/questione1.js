'use strict';

/**
 * @ngdoc function
 * @name jobquestApp.controller:Questione1Ctrl
 * @description
 * # Questione1Ctrl
 * Controller of the jobquestApp
 */
angular.module('jobquestApp')
    .controller('Questione1Ctrl', function ($stateParams,authToken,$scope,$http,$state) {
        var imagemAtual=0;
        $scope.pergunta={};
        $scope.bgClass=function(){
            if (imagemAtual<=4&&$stateParams.id==1){
                return "game-1-"+imagemAtual;
            }else{
                return "game-none";
            }
        }
        $scope.changeTexto = function(){
            imagemAtual++;
        }

        $scope.showTexto7=false;
        $scope.multiplaEscolha=true;
        $scope.remove = function() {
            $scope.showTexto7=false;
        };
        $scope.multAltSel=[];
        $scope.selectMult= function(valor){
            if($scope.multAltSel.indexOf(valor)>-1){
                var index =  $scope.multAltSel.indexOf(valor);
                $scope.multAltSel.splice(index, 1);
            }else{
                $scope.multAltSel.push(valor);
            }
        }
        $scope.verificaMult= function(valor){
            return $scope.multAltSel.indexOf(valor)>-1;
        }
        console.log($stateParams.id==1)
        $scope.questao1=$stateParams.id==1;
        $scope.questao2=$stateParams.id==2;
        $scope.alternativaSelecionada = {}
        $scope.pergunta={};
        $http.get('http://via.events/jogoquest/api/Perguntas/ObterPorId/2E1.'+$stateParams.id+'/'+authToken.getToken()).success(function(res){
            $scope.pergunta=res;
            console.log(JSON.stringify(res));
        })
        function sortNumber(a,b) {
            return a - b;
        }
        $scope.selecionaAlternativa = function(alternativa){
            $scope.alternativaSelecionada = alternativa;
        }
        $scope.enviar = function(){
            if($scope.alternativaSelecionada!={}){
                if($stateParams<=1){
                    $http.post('http://via.events/jogoquest/api/Perguntas/Responder/'+authToken.getToken(),
                        {
                            "NumeroPergunta":  $scope.pergunta.Numero,
                            "ValorAlternativaRespondida": $scope.alternativaSelecionada.Valor
                        }).success(function(){
                            if($stateParams.id!=3){
                                $state.go('questione1',{id:parseInt($stateParams.id)+1});
                            }else{
                                $state.go('home');
                            }
                        })
                }else{
                    var resposta = {
                        "NumeroPergunta":  $scope.pergunta.Numero,
                        "ValorAlternativaRespondida": $scope.multAltSel.sort().toString()
                    };
                    console.log(resposta)
                    $http.post('http://via.events/jogoquest/api/Perguntas/Responder/'+authToken.getToken(),
                            resposta).success(function(){
                            if($stateParams.id!=2){
                                $state.go('questione1',{id:parseInt($stateParams.id)+1});
                            }else{
                                $state.go('home');
                            }
                        })
                }

            }
        }

    });

'use strict';
