'use strict';

/**
 * @ngdoc function
 * @name jobquestApp.controller:RankingCtrl
 * @description
 * # RankingCtrl
 * Controller of the jobquestApp
 */
angular.module('jobquestApp')
  .controller('RankingCtrl', function ($scope,authToken,$http) {
   $scope.ranking={};
        $http.get('http://via.events/jogoquest/api/Ranking/ObterRanking/'+authToken.getToken()).success(function(res){
            console.log(res);
            $scope.ranking=res;
        })
  });
