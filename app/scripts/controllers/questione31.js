'use strict';

/**
 * @ngdoc function
 * @name jobquestApp.controller:Questione3Ctrl
 * @description
 * # Questione3Ctrl
 * Controller of the jobquestApp
 */
angular.module('jobquestApp')
  .controller('Questione31Ctrl',function ($scope,$http, $state,authToken) {
        var imagemAtual=1;
        $scope.pergunta={};
        $scope.bgClass=function(){
            if (imagemAtual<=2){
                return "game-3-"+imagemAtual;
            }else{
                return "game-none";
            }
        }
        $scope.changeTexto = function(){
            imagemAtual++;
        }
        $scope.showTexto6=true;
        $scope.remove = function() {
            $scope.showTexto6=false;
        };

        $scope.centerAnchor = true;
        $scope.toggleCenterAnchor = function () {$scope.centerAnchor = !$scope.centerAnchor}
        $scope.alternativas = [];
        $scope.questao1= [];
        $scope.questao2= [];
        $scope.questao3= [];
        $scope.questao4= [];
        $scope.questao5= [];
        $scope.questao6= [];
        $scope.questao7= [];
        $scope.questao8= [];
        $scope.questao9= [];
        $scope.questao10= [];
        $scope.questao11= [];
        $scope.questao12= [];
        $http.get('http://via.events/jogoquest/api/Perguntas/ObterPorId/2E3.1/'+authToken.getToken())
            .success(function(res){
                $scope.pergunta=res;
                console.log($scope.pergunta)

                $scope.alternativas= $scope.pergunta.Alternativas;
                angular.forEach($scope.alternativas,function(value, index){
                    value.insideContainer='alternativas';
                })
            })
        $scope.onDropAlternativasComplete=function(data,evt){
            var index = $scope.alternativas.indexOf(data);
            if (index == -1&&data){
                data.insideContainer='alternativas';
                $scope.alternativas.push(data);
            }
        }

        $scope.onDragAlternativasSuccess=function(data,evt){
            var index = $scope.alternativas.indexOf(data);
            console.log(data);
            if (index > -1) {
                $scope.alternativas.splice(index, 1);
            }
        }


        //####################################questao1#####################################
        $scope.onDragQuestao1Success=function(data,evt){
            var index = $scope.questao1.indexOf(data);
            if (index > -1) {
                $scope.questao1.splice(index, 1);
            }
        }
        $scope.onDropQuestao1Complete=function(data,evt){
            if($scope.questao1[0]&&data.insideContainer!='questao1'){
                $scope.questao1[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao1[0]);
                $scope.questao1.splice(0, 1);
            }
            data.insideContainer='questao1';
            $scope.questao1.push(data);
        }

        //########################questao2######################
        $scope.onDragQuestao2Success=function(data,evt){
            var index = $scope.questao2.indexOf(data);
            if (index > -1) {
                $scope.questao2.splice(index, 1);
            }
        }
        $scope.onDropQuestao2Complete=function(data,evt){
            if($scope.questao2[0]&&data.insideContainer!='questao2'){
                $scope.questao2[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao2[0]);
                $scope.questao2.splice(0, 1);
            }
            data.insideContainer='questao2';
            $scope.questao2.push(data);
        }



        //########################questao3######################
        $scope.onDragQuestao3Success=function(data,evt){
            var index = $scope.questao3.indexOf(data);
            if (index > -1) {
                $scope.questao3.splice(index, 1);
            }
        }
        $scope.onDropQuestao3Complete=function(data,evt){
            console.log(data);
            if($scope.questao3[0]&&data.insideContainer!='questao3'){
                console.log("executo isso?");
                $scope.questao3[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao3[0]);
                $scope.questao3.splice(0, 1);
            }
            data.insideContainer='questao3';
            $scope.questao3.push(data);
        }


//########################questao4######################
        $scope.onDragQuestao4Success=function(data,evt){
            var index = $scope.questao4.indexOf(data);
            if (index > -1) {
                $scope.questao4.splice(index, 1);
            }
        }
        $scope.onDropQuestao4Complete=function(data,evt){
            console.log("data.insideContainer")
            console.log(data.insideContainer)
            if($scope.questao4[0]&&data.insideContainer!='questao4'){
                console.log("executo isso?");
                $scope.questao4[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao4[0]);
                $scope.questao4.splice(0, 1);
            }
            data.insideContainer='questao4';
            $scope.questao4.push(data);
        }


        //########################questao5######################
        $scope.onDragQuestao5Success=function(data,evt){
            var index = $scope.questao5.indexOf(data);
            if (index > -1) {
                $scope.questao5.splice(index, 1);
            }
        }
        $scope.onDropQuestao5Complete=function(data,evt){
            console.log("data.insideContainer")
            console.log(data.insideContainer)
            if($scope.questao5[0]&&data.insideContainer!='questao5'){
                console.log("executo isso?");
                $scope.questao5[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao5[0]);
                $scope.questao5.splice(0, 1);
            }
            data.insideContainer='questao5';
            $scope.questao5.push(data);
        }

        //########################questao6######################
        $scope.onDragQuestao6Success=function(data,evt){
            var index = $scope.questao6.indexOf(data);
            if (index > -1) {
                $scope.questao6.splice(index, 1);
            }
        }
        $scope.onDropQuestao6Complete=function(data,evt){
            console.log("data.insideContainer")
            console.log(data.insideContainer)
            if($scope.questao6[0]&&data.insideContainer!='questao6'){
                console.log("executo isso?");
                $scope.questao6[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao6[0]);
                $scope.questao6.splice(0, 1);
            }
            data.insideContainer='questao6';
            $scope.questao6.push(data);
        }

        //########################questao2######################
        $scope.onDragQuestao7Success=function(data,evt){
            var index = $scope.questao7.indexOf(data);
            if (index > -1) {
                $scope.questao7.splice(index, 1);
            }
        }
        $scope.onDropQuestao7Complete=function(data,evt){
            console.log("data.insideContainer")
            console.log(data.insideContainer)
            if($scope.questao7[0]&&data.insideContainer!='questao7'){
                console.log("executo isso?");
                $scope.questao7[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao7[0]);
                $scope.questao7.splice(0, 1);
            }
            data.insideContainer='questao7';
            $scope.questao7.push(data);
        }


        $scope.onDragQuestao8Success=function(data,evt){
            var index = $scope.questao8.indexOf(data);
            if (index > -1) {
                $scope.questao8.splice(index, 1);
            }
        }
        $scope.onDropQuestao8Complete=function(data,evt){
            console.log("data.insideContainer")
            console.log(data.insideContainer)
            if($scope.questao8[0]&&data.insideContainer!='questao8'){
                console.log("executo isso?");
                $scope.questao8[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao8[0]);
                $scope.questao8.splice(0, 1);
            }
            data.insideContainer='questao8';
            $scope.questao8.push(data);
        }

        $scope.onDragQuestao9Success=function(data,evt){
            var index = $scope.questao9.indexOf(data);
            if (index > -1) {
                $scope.questao9.splice(index, 1);
            }
        }
        $scope.onDropQuestao9Complete=function(data,evt){
            console.log("data.insideContainer")
            console.log(data.insideContainer)
            if($scope.questao9[0]&&data.insideContainer!='questao9'){
                console.log("executo isso?");
                $scope.questao9[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao9[0]);
                $scope.questao9.splice(0, 1);
            }
            data.insideContainer='questao9';
            $scope.questao9.push(data);
        }

        $scope.onDragQuestao10Success=function(data,evt){
            var index = $scope.questao10.indexOf(data);
            if (index > -1) {
                $scope.questao10.splice(index, 1);
            }
        }
        $scope.onDropQuestao10Complete=function(data,evt){
            console.log("data.insideContainer")
            console.log(data.insideContainer)
            if($scope.questao10[0]&&data.insideContainer!='questao10'){
                console.log("executo isso?");
                $scope.questao10[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao10[0]);
                $scope.questao10.splice(0, 1);
            }
            data.insideContainer='questao10';
            $scope.questao10.push(data);
        }

        $scope.onDragQuestao11Success=function(data,evt){
            var index = $scope.questao11.indexOf(data);
            if (index > -1) {
                $scope.questao11.splice(index, 1);
            }
        }
        $scope.onDropQuestao11Complete=function(data,evt){
            console.log("data.insideContainer")
            console.log(data.insideContainer)
            if($scope.questao11[0]&&data.insideContainer!='questao11'){
                console.log("executo isso?");
                $scope.questao11[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao11[0]);
                $scope.questao11.splice(0, 1);
            }
            data.insideContainer='questao11';
            $scope.questao11.push(data);
        }

        $scope.onDragQuestao12Success=function(data,evt){
            var index = $scope.questao12.indexOf(data);
            if (index > -1) {
                $scope.questao12.splice(index, 1);
            }
        }
        $scope.onDropQuestao12Complete=function(data,evt){
            console.log("data.insideContainer")
            console.log(data.insideContainer)
            if($scope.questao12[0]&&data.insideContainer!='questao12'){
                console.log("executo isso?");
                $scope.questao12[0].insideContainer=data.insideContainer;
                eval('$scope.'+data.insideContainer).push($scope.questao12[0]);
                $scope.questao12.splice(0, 1);
            }
            data.insideContainer='questao12';
            $scope.questao12.push(data);
        }
        var inArray = function(array, obj) {
            var index = array.indexOf(obj);
        }
        $scope.responderE1= function(){
            var resposta =
            {
                "NumeroPergunta": "E1",
                "RespostasEspeciais": [
                    {
                        "IdGrupoAlternativa": "1",
                        "Resposta": ""
                    },
                    {
                        "IdGrupoAlternativa": "2",
                        "Resposta": ""
                    }
                ]
            }

            var resposta = "";
            resposta+=$scope.questao2[0].Valor;
            resposta+=",";
            resposta+=$scope.questao3[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.questao4[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.questao5[0].Valor;
            resposta+=",";
            resposta+=($scope.questao6[0].Valor==6||$scope.questao6[0].Valor==7)? '6':$scope.questao6[0].Valor;
            resposta+=",";
            resposta+=($scope.questao7[0].Valor==6||$scope.questao7[0].Valor==7)? '7':$scope.questao7[0].Valor;

            resposta+=","+$scope.questao8[0].Valor;
            console.log(resposta)
            resposta+=","+$scope.questao9[0].Valor;

            resposta+=","+$scope.questao10[0].Valor;
            resposta+=","+$scope.questao11[0].Valor;
            resposta+=","+$scope.questao12[0].Valor;

            var respostaPrincipal = {
                "NumeroPergunta":  $scope.pergunta.Numero,
                "ValorAlternativaRespondida": resposta
            };
            console.log(respostaPrincipal)
            $http.post('http://via.events/jogoquest/api/Perguntas/Responder/'+authToken.getToken(),
                    respostaPrincipal).success(function(){
                    $state.go('home');

                })

        }

    });
